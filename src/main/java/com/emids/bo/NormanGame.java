package com.emids.bo;

import java.util.Arrays;

public class NormanGame {

	private String name;
	private String gender;
	private Integer age;
	private String[] health;
	private String[] habits;
	
	public NormanGame() {
	}

	public NormanGame(String name, String gender, Integer age, String[] health, String[] habits) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.health = health;
		this.habits = habits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String[] getHealth() {
		return health;
	}

	public void setHealth(String[] health) {
		this.health = health;
	}

	public String[] getHabits() {
		return habits;
	}

	public void setHabits(String[] habits) {
		this.habits = habits;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NormanGame [name=");
		builder.append(name);
		builder.append(", gender=");
		builder.append(gender);
		builder.append(", age=");
		builder.append(age);
		builder.append(", health=");
		builder.append(health);
		builder.append(", habits=");
		builder.append(habits);
		builder.append("]");
		return builder.toString();
	}
	
}
